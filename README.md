# SolidityChallenge

```
Can you prove within transaction runtime that particular event with particular
set of arguments has been emitted on the contract in the past? 
If yes, how (general concept).
```
```
No, We cant do that during transaction runtime
A contract has access to the current state of the blockchain, and transactions are not part of state.
```
---------------------------------------------------------------------------------------------------------

```
Can you verify that the there is a contract deployed at some Ethereum address 
with specific bytecode within Ethereum transaction context 
(i.e. you want to make sure that there is the trusted contract you need at some address
with some predefined source code and not some malicious code before calling it)?
```
```
I can verify using assembly code with in the solidity function

EXTCODESIZE:
This opcode returns the size of the code on an address.
If the size is larger than zero, the address is a contract.
```
```js
 function isAContract(address _address) private returns (bool isAContract){
 uint32 size;
 assembly {
   size := extcodesize(_address)
 }
 return (size > 0);
}
```
---------------------------------------------------------------------------------------------------------

```
If your contract is calling some other contract’s function,
 - can this other contract’s function return a struct data structure ?
 - what are your options for it?
```
```
As per solidity-readthedocs, struct return is for only internal function calls.

Alternatives:

For external/public contract calls, in this question, other contract's funcction,

You can use structs with a library

OR

If you do not want to use libraries you can create an abstract contract,
that only contains the structs an inherit from them

OR

Use pragma experimental ABIEncoderV2;
```
---------------------------------------------------------------------------------------------------------


Add code to calculate storage address for value at address:

To find the location of a specific value within a mapping, the key and the mapping’s slot are hashed together.

location of value: uint256(keccak256(key, slot))

```js
contract A {
  mapping (address => uint256) values;  // slot 0, data at h(k . 0)

  function getValue(address _addr) public view returns (bytes32 storagePointer) {
    assembly {
            storagePointer := keccak256(_addr, 0)
    }
  }
}
```

---------------------------------------------------------------------------------------------------------
Given the following set of contracts the goal is to implement to-bytes and from-bytes converter for structs to be able to pass them from contract to contract in a form of bytes. Please assume that Storage and Controller contracts have different addresses on the network. StructDefiner is never deployed on its own and is used as a part of Storage or Controller.

```
Solution approach:
serialize each property in struct and construct a sequential bytes
utilize assembly approach to serialize each property
uint256toBytes, addressToBytes , uint128ToBytes

append the serialized content next to each other

Follow similar approach for deserialization
```


```js
pragma solidity ^0.4.24;
pragma experimental ABIEncoderV2;

contract StructDefiner {
  struct MyStruct {
    uint256 someField;
    address someAddress;
    uint128 someOtherField;
    uint128 oneMoreField;
  }
}

contract Storage {
  StructDefiner.MyStruct[] internal structs; //array at slot-0 and contains sizeOf(structs)
  //The values in the array are stored consecutively starting at the hash of the slot.

  function getStructByIdx(uint256 idx) external view returns (bytes) {
    
    //declare and initialize buffer
    bytes memory dataAsBytes = new bytes(84);
    
    uint elementSize = 84;
    
    StructDefiner.MyStruct memory structElement;
    
     assembly {
    
        // The length is loaded (first 32 bytes)
        let len := mload(structs_slot)
       
        let dataArray := add(structs_slot, 0x20)
        
        //identify location of the element in array (from index)
        let arrayLocation := add(keccak256(0,0), mul(idx, elementSize))
        
        structElement := mload(add(dataArray,arrayLocation))
        
        //sizeOf each structElement
        let someField := mload(add(structElement, 32))
        let someAddress := mload(add(structElement, 52))
        let someOtherField := mload(add(structElement, 64))
        let oneMoreField := mload(add(structElement, 86))

        mstore(add(dataAsBytes, 32), someField)
        mstore(add(dataAsBytes, 52), someAddress)
        mstore(add(dataAsBytes, 64), someOtherField)
        mstore(add(dataAsBytes, 86), oneMoreField)

    }
    
    return dataAsBytes;
  }
}

contract Controller {
  Storage internal storageInstance;

   constructor(address _storage) public{
    storageInstance = Storage(_storage);
  }

  function getStruct(uint256 idx) public returns (StructDefiner.MyStruct myStruct) {
    bytes memory _myStruct = storageInstance.getStructByIdx(idx);
    StructDefiner.MyStruct memory newEntity;
    uint256 someField;
    address someAddress;
    uint128 someOtherField;
    uint128 oneMoreField;

    assembly {
         someField := mload(add(_myStruct, 32))
         someAddress := mload(add(_myStruct, 52))
         someOtherField := mload(add(_myStruct, 64))
         oneMoreField := mload(add(_myStruct, 86))
    }
    
    newEntity.someField = someField;
    newEntity.someAddress = someAddress;
    newEntity.someOtherField = someOtherField;
    newEntity.oneMoreField = oneMoreField;

    return newEntity;
  }
}  

```
